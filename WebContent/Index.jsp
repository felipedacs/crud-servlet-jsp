<%@ page language="java" import="java.util.*" import="Models.Produto" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Index</title>
<%@ include file="Components/head.jsp" %>  
</head>
<body>
<%@ include file="Components/header.jsp" %>
<div class="container">
	<h1>INDEX</h1>
	
	<table class="table">
	<% ArrayList<Produto> produtos = (ArrayList<Produto>)request.getAttribute("produtos");
	for (Produto produto : produtos) { %>
		<tr>
			<td><%=produto.getID()%></td>
			<td><strong><%=produto.getNome()%></strong></td>
			<td><%=produto.getValor()%></td>
			<td><a href="http://localhost:8080/CRUD/Details?id=<%=produto.getID()%>">Detalhes</a></td>
			<td><a href="http://localhost:8080/CRUD/Edit?id=<%=produto.getID()%>">Editar</a></td>
			<td><a href="http://localhost:8080/CRUD/Delete?id=<%=produto.getID()%>">Apagar</a></td>
		</tr>
		<%}%>
	</table>
</div>

</body>
</html>