<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Create</title>
<%@ include file="Components/head.jsp" %>  
</head>
<body>
<%@ include file="Components/header.jsp" %>
<div class="container">
	<h1>CREATE</h1>
	<form action="http://localhost:8080/CRUD/Create" method = "post">
		
			<input class="form-control" type="hidden" name="id">
		<div class="form-group">
		    <label>Nome</label>
			<input class="form-control" type="text" name="nome">
		</div>
		<div class="form-group">
		    <label>Valor</label>
			<input class="form-control" type="number" name="valor">
		</div>
	<input type="submit" class="btn btn-success">
	</form>
</div>
</body>
</html>