<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Edit</title>
<%@ include file="Components/head.jsp" %>  
</head>
<body>
<%@ include file="Components/header.jsp" %>
<div class="container">
	<h1>EDIT</h1>
	<form action="http://localhost:8080/CRUD/Edit" method = "post">
		
		<input class="form-control" value="${produto.getID()}" type="hidden" name="id">
		
		<div class="form-group">
		    <label>Nome</label>
			<input class="form-control" value="${produto.getNome()}" type="text" name="nome">
		</div>
		<div class="form-group">
		    <label>Valor</label>
			<input class="form-control" value="${produto.getValor()}" type="number" name="valor">
		</div>
		<input type="submit" class="btn btn-success" value="Confirmar">
	</form>
</div>
</body>
</html>