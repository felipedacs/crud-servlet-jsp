<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Details</title>
<%@ include file="Components/head.jsp" %>  
</head>
<body>
<%@ include file="Components/header.jsp" %>
<div class="container">
	<h1>DETAILS</h1>
	<form action="http://localhost:8080/CRUD/Create" method = "post">
		<div class="form-group">
		    <label>Id</label>
			<input class="form-control" value="${produto.getID()}" type="text" name="id" disabled>
		</div>
		<div class="form-group">
		    <label>Nome</label>
			<input class="form-control" value="${produto.getNome()}" type="text" name="nome" disabled>
		</div>
		<div class="form-group">
		    <label>Valor</label>
			<input class="form-control" value="${produto.getValor()}" type="number" name="valor" disabled>
		</div>
	</form>
</div>
</body>
</html>