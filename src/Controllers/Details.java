package Controllers;


import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Models.Produto;
import Repositories.ProdutoRepositoryArray;

/**
 * Servlet implementation class Details
 */
@WebServlet("/Details")
public class Details extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Details() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (request.getParameter("id") == null) {
			request.getRequestDispatcher("/Index").forward(request, response);			
		} else {
			int id = Integer.parseInt(request.getParameter("id"));
			ProdutoRepositoryArray repo = new ProdutoRepositoryArray();
			Produto produto = repo.Details(id);
			request.setAttribute("produto", produto);
			request.getRequestDispatcher("/Details.jsp").forward(request, response);
		}
	}
}
