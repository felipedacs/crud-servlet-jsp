package Repositories;

import Models.Produto;

public interface IProdutoRepository extends IRepositoryBase<Produto> {

	Produto Details(int id);

}
