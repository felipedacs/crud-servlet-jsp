package Models;

public class Produto {
	private int ID;
	private String nome;
	private double valor;
	
	
	
	public Produto(String name, double valor) {
		super();
		this.nome = name;
		this.valor = valor;
	}
	
	public Produto(String name, double valor, int ID) {
		super();
		this.ID = ID;
		this.nome = name;
		this.valor = valor;
	}
	
	
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String name) {
		this.nome = name;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	
	

}
